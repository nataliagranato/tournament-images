FROM httpd:2.4.46
COPY app /usr/local/apache2/htdocs/app/
RUN chmod +x /usr/local/apache2/htdocs/app/index.html
RUN cd /usr/local/apache2/htdocs/ && \
    ls -lh
CMD httpd-foreground -c "LoadModule cgid_module modules/mod_cgid.so"
EXPOSE 8080
WORKDIR /usr/local/apache2/cgi-bin
